#pragma once
#include "Helper.h"
#include<Windows.h>
#include <iostream>
#include "Room.h"
#include "Game.h"

#define EROR_VALUE -1

using namespace std;

class Room;
class Game;
class User
{
public:
	User(string username, SOCKET sock);
	~User();
	void send(string message);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* game);
	void clearRoom();
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();



private:
	string _username;
	Room* _currRoom;
	SOCKET _sock;
	Game* _currGame;

};

