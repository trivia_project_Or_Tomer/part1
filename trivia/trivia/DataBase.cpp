#include "DataBase.h"

#include <map>

map < string, pair<string, string>> t_users;
map <int, pair<int, pair<time_t, time_t>>> t_games;
map <int, string *> t_questions;
map <int, pair<string, pair<int, pair<string, pair<int, int>>>>> t_players_answers;

DataBase::DataBase()
{
	t_questions.insert(pair<int,string *>(0, new string[5]{ "question" , "answer1", "answer2", "answer3", "answer4" }));
	t_questions.insert(pair<int, string *>(1, new string[5]{ "what is the best gaming system?" , "pc", "xbox", "playstation4", "wii" }));
	t_questions.insert(pair<int, string *>(2, new string[5]{ "which is an Unnecessary institiotion?" , "school", "collage", "google", "the white house" }));
	t_questions.insert(pair<int, string *>(3, new string[5]{ "When should you go?" , "Mars", "The sun", "The bottom of the ocean", "Arad" }));
	t_questions.insert(pair<int, string *>(4, new string[5]{ "Best social media" , "whatsapp", "instegram", "grinder", "Facebook" }));
	t_questions.insert(pair<int, string *>(5, new string[5]{ "Israel decleration happend at ....." , "1948", "2020", "1520", "7900" }));
	t_questions.insert(pair<int, string *>(6, new string[5]{ "The democracy was invented by" , "The Greeks", "USA", "Franch", "some arab country" }));
	t_questions.insert(pair<int, string *>(7, new string[5]{ "What is the name of the Greek god who rule the Sheol" , "Hades", "Zeus", "Nepton", "Dean Kamen" }));
	t_questions.insert(pair<int, string *>(8, new string[5]{ "Strongest superHero" , "Superman", "Batman", "Spiderman", "The Arctic man" }));
	t_questions.insert(pair<int, string *>(9, new string[5]{ "It's one small step for man one giant leap for" , "mankind", "Hulk", "Marines", "The phoenix" }));
	t_questions.insert(pair<int, string *>(10, new string[5]{ "Name of the USA president in 2018" , "trump", "Bin laden", "abraham lincoln", "Bibi" }));

}
DataBase::~DataBase()
{
	t_users.clear();
	t_games.clear();
	t_questions.clear();
	t_players_answers.clear();
}

bool DataBase::isUserExists(string username)
{
	return t_users.count(username);
}

bool DataBase::addNewUser(string user, string password, string email)
{
	t_users.insert(pair<string,pair<string,string>>(user, pair<string, string>(password, email)));
	return true;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	for (auto it = t_users.begin(); it != t_users.end(); it++)
	{
		if (it->first == username)
		{
			if (it->second.first == password)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	return false;
}

vector<Question*> DataBase::initQuestions(int questionsNumber)
{
	srand(time(NULL));
	vector<Question * > questions;
	int * numbers = new int[questionsNumber] {NULL};
	int place = NULL, counter = NULL;
	while(counter <  questionsNumber)
	{
		place = rand() % (t_questions.size());
		if (isNotExits(numbers, questionsNumber, place))
		{
			questions.push_back(new Question(place, 0, t_questions[place][0], t_questions[place][1], t_questions[place][2], t_questions[place][3], t_questions[place][4]));
			numbers[counter] = place;
			counter++;
		}
	}

	return questions;
}
bool DataBase::isNotExits(int * arr, int size, int value)
{
	for (int j = 0; j < size; j++)
	{
		if (arr[j] == value)
		{
			return false;
		}
	}
	return true;
}

int DataBase::insetNewGame()
{
	t_games.insert(pair<int, pair<int, pair<time_t, time_t>>>
		(t_games.size(), pair<int, pair<time_t, time_t>>(0, pair<time_t,time_t>(time(0),NULL))));
	return t_games.size() - 1;
}

bool DataBase::updateGameStatus(int gameId)
{
	std::map<int, pair<int, pair<time_t, time_t>>>::iterator it = t_games.find(gameId);
	it->second.first = 1;
	it->second.second.second = time(0);

	return false;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int qusetionId, string answer, bool isCorrect, int answerTime)
{
	t_players_answers.insert(pair<int, pair<string, pair<int, pair<string, pair<int, int>>>>>
		(gameId, pair<string, pair<int, pair<string, pair<int, int>>>>(username,
			pair<int, pair<string, pair<int, int>>>(qusetionId,
				pair<string, pair<int, int>>(answer, pair<int, int>(isCorrect, answerTime))))));
	return true;
}
