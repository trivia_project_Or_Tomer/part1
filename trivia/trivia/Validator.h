#pragma once
#include <iostream>

using namespace std;


class Validator
{
public:
	Validator();
	~Validator();
	static bool isPasswordValid(string password);
	static bool isUsernameValid(string userName);
private:
	static bool isAllBoolTrue(bool * arr, int size);
};

