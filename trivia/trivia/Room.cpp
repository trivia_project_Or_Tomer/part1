#include "Room.h"






Room::Room(int id, User * admin, string name, int maxUsers, int questionNo, int questionTime)
{
	this->_maxUsers = maxUsers;
	this->_questionNo = questionNo;
	this->_questionTime = questionTime;
	this->_admin = admin;
	this->_name = name;
	this->_id = id;
	this->_users.push_back(admin);
}

Room::~Room()
{
}

bool Room::joinRoom(User * user)
{
	bool isJoined = false;
	string response;
	if (this->_maxUsers != this->_users.capacity()) //Room yet to be full
	{
		this->_users.push_back(user);
		isJoined = true;
		response = to_string(MESSAGES_CODE::JOIN_ROOM_ANSWER_SUCCESS)
			+ Helper::getPaddedNumber(this->_questionNo, 2)
			+ Helper::getPaddedNumber(this->_questionTime, 2);
		Helper::sendData(user->getSocket(), response);
		//send 108 request to all
		updateUsers();
	}
	else
	{
		//send an unsucessfull joining request
		Helper::sendData(user->getSocket(), to_string(MESSAGES_CODE::JOIN_ROOM_ANSWER_FAILURE_ROOM_IS_FULL));
	}

	return isJoined;
}

void Room::leaveRoom(User * user)
{
	if (std::find(this->_users.begin(), this->_users.end(), user) != this->_users.end())
	{
		this->_users.erase(std::remove(this->_users.begin(), this->_users.end(), user), this->_users.end());
		//After remove user from the system we send him a sucsessful leave messege
		Helper::sendData(user->getSocket(), "1120");
		updateUsers();

	}

}

int Room::closeRoom(User * user)
{
	int returnValue = -1;

	if (user != this->_admin)
	{
		return returnValue;
	}
	else
	{
		returnValue = this->_id;
	}


	for (std::vector<User*>::iterator it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		//send closing messege to current user in loop
		Helper::sendData((*it)->getSocket(), "116");
		//active the clear room function on each object
		(*it)->clearRoom();

	}

	return returnValue;
}



vector<User*> Room::getUsers()
{
	return this->_users;
}

std::string Room::getUsersListMessage()
{
	string listOfUsers = to_string(MESSAGES_CODE::SEND_USER_LIST_IN_ROOM);

	listOfUsers += std::to_string(this->_users.size());
	for (std::vector<User*>::iterator it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		listOfUsers += Helper::getPaddedNumber((*it)->getUsername().size(), 2)
			+ (*it)->getUsername();
	}
	return listOfUsers;
}

int Room::getQuestionsNo()
{
	return this->_questionNo;
}

int Room::getId()
{
	return this->_id;
}

string Room::getName()
{
	return this->_name;
}

void Room::updateUsers()
{
	string msg = getUsersListMessage();

	for (std::vector<User*>::iterator it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		Helper::sendData((*it)->getSocket(), msg);
	}
}
