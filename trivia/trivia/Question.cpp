#include "Question.h"





Question::Question(int id, int correctAnswerIndex, string question, string answer1, string answer2, string answer3, string answer4)
{
	int counter = 0, place = NULL;
	this->_id = id;
	this->_question = question;
	srand(time(NULL));
	while (counter < 3)
	{
		place = rand() % 4;
		if ("" == this->_answers[place])
		{
			if (0 == counter)
			{
				this->_answers[place] = answer1;
				if (0 == correctAnswerIndex)
				{
					this->_correctAnswerIndex = place;
				}
			}
			else if (1 == counter)
			{
				this->_answers[place] = answer2;
				if (1 == correctAnswerIndex)
				{
					this->_correctAnswerIndex = place;
				}
			}
			else if (2 == counter)
			{
				this->_answers[place] = answer3;
				if (2 == correctAnswerIndex)
				{
					this->_correctAnswerIndex = place;
				}
			}
			counter++;
		}
	}
	for (int i = 0; i < this->_answers->size(); i++)
	{
		if ("" == this->_answers[i])
		{
			this->_answers[i] = answer4;
			if (3 == correctAnswerIndex)
			{
				this->_correctAnswerIndex = place;
			}
			break;
		}

	}
	if (4 == correctAnswerIndex)
	{
		throw "correctAnswerIndex = 4";
	}
}

Question::~Question()
{
}

string Question::getQuestion()
{
	return this->_question;
}

string * Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}

int Question::getId()
{
	return this->_id;
}
