#pragma once
#include <iostream>
#include <vector>
#include "Game.h"
#include "Question.h"
#include <chrono>


using namespace std;

class Game;
class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string username);
	bool addNewUser(string user, string password, string email);
	bool isUserAndPassMatch(string username, string password);
	vector<Question*> initQuestions(int questionsNumber);
	int insetNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, string username, int qusetionId, string answer, bool isCorrect, int answerTime);
private:
	bool isNotExits(int * arr, int size, int value);






};