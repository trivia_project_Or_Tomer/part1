#pragma once
#include <WinSock2.h>
#include <vector>
#include <string>

enum MESSAGES_CODE
{
	//FROM CLIENT
	SIGN_IN_SECSUSS = 1020,
	SIGN_IN_FAILURE_WRONG_DETAILS,
	SIGN_IN_FAILURE_ALREADY_CONNECTED,
	SIGN_UP_SECSUSS = 1040,
	SIGN_UP_FAILURE_ILLEGAL_PASSWORD,
	SIGN_UP_FAILURE_ALREADY_EXISTS,
	SIGN_UP_FAILURE_ILLEGAL_USERNAME,
	SIGN_UP_FAILURE_OTHER_FAILURE,
	SEND_ROOM_LIST = 106,
	SEND_USER_LIST_IN_ROOM = 108,
	JOIN_ROOM_ANSWER_SUCCESS = 1100,
	JOIN_ROOM_ANSWER_FAILURE_ROOM_IS_FULL,
	JOIN_ROOM_ANSWER_FAILURE_ROOM_NOT_EXITS_OR_OTHER,
	LEAVE_ROOM_RESPONSE = 1120,
	CREATE_NEW_ROOM_SUCCESS = 1140,
	CREATE_NEW_ROOM_FAILURE,
	CLOSE_ROOM_RESPONSE = 116,
	SEND_QUESTION = 118,
	CREATE_GAME_FALIURE = 1180,
	RIGHT_ENSWER_RESPONSE = 120,
	FINISH_GAME = 121,
	BEST_SCORES_RESPONSE = 124,
	PERSONAL_STATE_RESPONSE = 126,

	// FROM CLIENT
	SIGN_IN = 200,
	SIGN_OUT = 201,
	SIGN_UP = 203,
	ROOM_LIST = 205,
	UESR_IN_ROOM_LIST = 207,
	JOIN_ROOM = 209,
	LEAVE_ROOM = 211,
	CREATE_ROOM = 213,
	CLOSE_ROOM = 215,
	START_GAME = 217,
	ANSWER_RECIVED_FORM_USER = 219,
	LEAVE_GAME = 222,
	BEST_SCORES = 223,
	PERSONAL_STATE = 225,
	EXIT_APP = 299


};

#pragma comment (lib, "Ws2_32.lib")



class Helper
{
public:



	static int getMessageTypeCode(SOCKET sc);
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);
	static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	static std::string getStringPartFromSocket(SOCKET sc, int bytesNum);
	static void sendData(SOCKET sc, std::string message);
	static std::string getPaddedNumber(int num, int digits);

private:
	static char* getPartFromSocket(SOCKET sc, int bytesNum);


};


#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#define TRACE(msg, ...) // do nothing
#endif