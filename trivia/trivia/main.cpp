#include "WSAInitializer.h"
#include "TriviaServer.h"
#include <string>
#include <exception>


void main()
{
	try
	{
		TriviaServer * game = new TriviaServer();
		game->server();
		delete game;
	}
	catch (char * eror)
	{
		cout << "EROR - " << eror << endl;
	}
	catch (exception eror)
	{
		cout << "EROR	-	" << eror.what() << endl;
	}
	catch (...)
	{
		cout << "an exception has occurred" << endl;
	}

}


