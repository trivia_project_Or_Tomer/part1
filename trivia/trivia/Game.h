#pragma once
#include <WinSock2.h>
#include <iostream>
#include <vector>
#include <iomanip> 
#include <sstream>
#include <map>
#include "DataBase.h"
#include "Question.h"
#include "User.h"


using namespace std;

class User;
class DataBase;
class Game
{
public:
	Game(const vector<User*>& players, int questionsNumber, DataBase& db);
	~Game();
	void sendQuestionToAllUsers();
	void handleFinishGame();
	void sendFirstQuestion();
	bool handleNextTurn();
	bool handleAnswerFromUser(User * user, int answerNumber, int time);
	bool leaveGame(User * currUser);


private:

	vector<Question *> _questions;
	vector<User*> _players;
	int _questionsNumber;
	int _currQuestionIndex;
	DataBase & _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	int _gameId;
};








