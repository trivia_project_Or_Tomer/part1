#pragma once
#include <mutex>
#include <condition_variable>
class ThreadController
{
public:
	std::mutex _mtx;
	std::condition_variable _cv;
	std::unique_lock<std::mutex> locker;
};