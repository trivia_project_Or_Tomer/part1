#pragma once



#include "User.h"
#include <algorithm>
#include <vector>




class User;
class Room
{
public:
	Room(int id, User* admin, std::string name, int maxUsers, int questionNo, int questionTime);
	~Room();
	//static void sendData(SOCKET sc, std::string message);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	std::vector<User*> getUsers();
	std::string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	std::string getName();
	void updateUsers();

private:
	std::vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	std::string _name;
	int _id;

	std::string getUsersAsString(std::vector<User*> usersList, User* excludeUser);
	void sendMessage(std::string message);
	void sendMessage(User* excludeUser, std::string message);
};

