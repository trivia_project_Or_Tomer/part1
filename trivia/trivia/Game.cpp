#include "Game.h"

#define EROR_VALUE -1

Game::Game(const vector<User*>& players, int questionsNumber, DataBase & db) : _db(db)
{
	this->_gameId = this->_db.insetNewGame();
	if (EROR_VALUE == this->_gameId)
	{
		throw exception("Eror in game constractor");
	}
	this->_questionsNumber = questionsNumber;
	this->_players = players;
	this->_questions = this->_db.initQuestions(questionsNumber);
	for (vector<User *>::iterator it = this->_players.begin(); it != this->_players.end(); it++)
	{
		(*it)->setGame(this);
		this->_results.insert(pair<string, int>((*it)->getUsername(), NULL));
	}
	this->_currQuestionIndex = 0;
}

Game::~Game()
{
	delete this->_players[0]->getRoom();
	for (auto it = this->_players.begin(); it != this->_players.end(); it++)
	{
		(*it)->clearRoom();
	}
	for (auto it = this->_questions.begin(); it != this->_questions.end(); it++)
	{
		delete (*it);
	}
}

void Game::sendQuestionToAllUsers()
{
	this->_currentTurnAnswers = 0;
	string msg = to_string(MESSAGES_CODE::SEND_QUESTION);
	Question * question = this->_questions[this->_currQuestionIndex];
	msg += Helper::getPaddedNumber(question->getQuestion().size(), 3) + question->getQuestion();
	for (int i = 0; i < NUMBER_OF_ANSWERS; i++)
	{
		msg += Helper::getPaddedNumber(question->getAnswers()[i].size(), 3) + question->getAnswers()[i];
	}
	for (vector<User *>::iterator it = this->_players.begin(); it != this->_players.end(); it++)
	{
		try
		{
			(*it)->send(msg);
		}
		catch (...)
		{
			cout << "Eror sending message" + to_string(MESSAGES_CODE::SEND_QUESTION) + " to " + (*it)->getUsername();
		}
	}


}

void Game::handleFinishGame()
{
	string msg = to_string(MESSAGES_CODE::FINISH_GAME) + Helper::getPaddedNumber(this->_results.size(), 1);
	this->_db.updateGameStatus(this->_gameId);

	for (map<string, int>::iterator it = this->_results.begin(); it != this->_results.end(); it++)
	{
		msg += Helper::getPaddedNumber(it->first.size(), 2) + it->first + Helper::getPaddedNumber(it->second, 2);

	}
	for (vector<User *>::iterator it = this->_players.begin(); it != this->_players.end(); it++)
	{
		try
		{
			(*it)->send(msg);
		}
		catch (...)
		{
			cout << "Error sending message" + to_string(MESSAGES_CODE::FINISH_GAME) + " to " + (*it)->getUsername();
		}
	}

}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();

}

bool Game::handleAnswerFromUser(User * user, int answerNumber, int time)
{
	this->_currentTurnAnswers++;
	if (answerNumber - 1 == this->_questions[this->_currQuestionIndex]->getCorrectAnswerIndex())
	{
		this->_results.find(user->getUsername())->second++;
		this->_db.addAnswerToPlayer(this->_gameId,
			user->getUsername(), this->_questions[this->_currQuestionIndex]->getId(),
			this->_questions[this->_currQuestionIndex]->getAnswers()[(answerNumber - 1)], true, time);
		user->send(to_string(MESSAGES_CODE::RIGHT_ENSWER_RESPONSE) + "1");
	}
	else if (5 == answerNumber)
	{
		this->_db.addAnswerToPlayer(this->_gameId, user->getUsername(),
			this->_questions[this->_currQuestionIndex]->getId(), "", false, time);
		user->send(to_string(MESSAGES_CODE::RIGHT_ENSWER_RESPONSE) + "0");
	}
	else
	{
		this->_db.addAnswerToPlayer(this->_gameId,
			user->getUsername(), this->_questions[this->_currQuestionIndex]->getId(),
			this->_questions[this->_currQuestionIndex]->getAnswers()[(answerNumber - 1)], false, time);
		user->send(to_string(MESSAGES_CODE::RIGHT_ENSWER_RESPONSE) + "0");
	}
	return handleNextTurn();
}

bool Game::leaveGame(User * currUser)
{
	for (vector<User *>::iterator it = this->_players.begin(); it != this->_players.end(); it++)
	{
		if ((*it)->getUsername() == currUser->getUsername())
		{
			this->_players.erase(it);
		}
	}
	return handleNextTurn();
}

bool Game::handleNextTurn()
{
	if (this->_currentTurnAnswers == this->_players.size())
	{
		if (this->_currQuestionIndex + 1 == this->_questions.size())
		{
			this->handleFinishGame();
			return false;
		}
		else
		{
			this->_currQuestionIndex++;
			sendQuestionToAllUsers();
		}
		
	}
	return true;
}

