#pragma once
#include <WinSock2.h>
#include <iostream>
#include <stdlib.h>    
#include <time.h>

#define NUMBER_OF_ANSWERS 4

using namespace std;


class Question
{
public:
	Question(int id, int correctAnswerIndex, string question, string answer1, string answer2, string answer3, string answer4);
	~Question();
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	int _correctAnswerIndex;
	int _id;
	string _question;
	string _answers[4]{ "" };

};

