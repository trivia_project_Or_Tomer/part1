#include "TriviaServer.h"
#pragma comment (lib, "ws2_32.lib")



#define DEFAULT_PORT 8820
#define END_COMM 0
#define BYTES_NUM 2

int TriviaServer::_roomIdSequenc = 0;

TriviaServer::TriviaServer() 
{
	WSADATA data;
	WSAStartup(MAKEWORD(2, 2), &data);
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == this->_socket)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

TriviaServer::~TriviaServer()
{
	closesocket(this->_socket);
	for (auto it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		delete it->second;
	}
	this->_connectUser.clear();
	for (auto it = this->_roomList.begin(); it != this->_roomList.end(); it++)
	{
		delete it->second;
	}
	WSACleanup();
	this->_roomList.clear();
}

void TriviaServer::server()
{
	thread messagesHandler(&TriviaServer::handleRecivedMessages, this);
	messagesHandler.detach();
	bindAndListen();

}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(DEFAULT_PORT); 
	sa.sin_family = AF_INET;   
	sa.sin_addr.s_addr = INADDR_ANY;   

	if (::bind(this->_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
	cout << "binded" << endl;
	if (::listen(this->_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	cout << "listening (Port = " + to_string(DEFAULT_PORT) + ")..." << endl;
	while (true)
	{
		accept();
	}


}

void TriviaServer::accept()
{
	cout << "accepting client..." << endl;
	SOCKET client_socket = ::accept(this->_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	cout << "Client accepted. Client socket = " << client_socket << endl;
	thread client(&TriviaServer::clientHendler,this, client_socket);
	client.detach();
}

void TriviaServer::clientHendler(SOCKET client_socket)
{

	int code = NULL;
	try
	{
		unique_lock<mutex> locker(this->_socketsMtx);
		locker.unlock();
		code = Helper::getMessageTypeCode(client_socket);
		do
		{
			addRecievedMessage(buildRecivedMessage(client_socket, code));
			Sleep(10);
			//this->_socketsCv.wait(locker, [client_socket] (){return client_socket == comparisonSock; });
			locker.lock();
			this->_socketsCv.wait(locker, [client_socket, this]() {return this->_comparisonSock == client_socket; });
			locker.unlock();
			this->_comparisonSock = NULL;
			code = Helper::getMessageTypeCode(client_socket);
			
		}
		while (code != END_COMM && EXIT_APP != code);
		addRecievedMessage(buildRecivedMessage(client_socket, END_COMM)); // creating an end comm message 
	}
	catch (...)
	{
		addRecievedMessage(buildRecivedMessage(client_socket, END_COMM));
	}




}

void TriviaServer::handleRecivedMessages()
{
	RecievedMessage * msg = nullptr;
	User * user = nullptr;
	while (nullptr ==  msg || msg->getMessageCode() != END_COMM)
	{
		unique_lock <mutex> locker(this->_queueMtx);
		if (this->_queRcvMessages.empty())
		{
			this->_queueCv.wait(locker);
		}
		//unique_lock<mutex> socketLocker(this->_socketsMtx);
		msg = this->_queRcvMessages.front();
		this->_queRcvMessages.pop();
		locker.unlock();
		cout << "--------------------------------------" << endl;
		cout << "handleRecievedMessages: msgCode = " << msg->getMessageCode();
		cout << ", client_socket: " << msg->getSock() << endl;
		unique_lock<mutex> socketLocker(this->_socketsMtx);
		//this->_socketsMtx.lock();
		switch (msg->getMessageCode())
		{
		case MESSAGES_CODE::SIGN_IN:
			user = handleSingin(msg); //check here
			if (nullptr != user)
			{
				this->_connectUser.insert(pair<SOCKET, User *>(msg->getSock(), user));
			}
			break;
		case MESSAGES_CODE::SIGN_OUT:
			handleSignout(msg); //check here
			break;
		case MESSAGES_CODE::SIGN_UP:
			handleSignup(msg); //check here
			break;
		case MESSAGES_CODE::CREATE_ROOM:
			handleCreateRoom(msg);
			break;
		case MESSAGES_CODE::CLOSE_ROOM:
			this->handleCloseRoom(msg);
			break;
		case MESSAGES_CODE::JOIN_ROOM:
			this->handleJoinRoom(msg);
			break;
		case MESSAGES_CODE::LEAVE_ROOM:
			this->handleLeaveRoom(msg);
			break;
		case MESSAGES_CODE::UESR_IN_ROOM_LIST:
			this->handleUserInRoom(msg);
			break;
		case MESSAGES_CODE::ROOM_LIST:
			this->handleGetRooms(msg);
			break;
		case MESSAGES_CODE::LEAVE_GAME:
			handleLeaveGame(msg);
			break;
		case MESSAGES_CODE::START_GAME:
			handleStartGame(msg);
			break;
		case MESSAGES_CODE::ANSWER_RECIVED_FORM_USER:
			handlePlayerAnswer(msg);
			break;
		case END_COMM:
			cout << "end comm with socket->" << msg->getSock() << "from user:" << msg->getUser();
			this->_connectUser.erase(msg->getSock());
			break;
		default:
			cout << "\n\n\n\n\nproblem!!!!!!!!!!!\n\n\n\n\n\n" << endl;
			break;
		}
		this->_comparisonSock = msg->getSock();
		//this->_socketsMtx.unlock();
		socketLocker.unlock();
		this->_socketsCv.notify_all();
		//socketLocker.unlock();
		/*
		if (socketLocker.owns_lock())
		{
		socketLocker.unlock(); // THROWS AN exception not sure why
		}
	*/


	}

}




void TriviaServer::safeDeleteUser(RecievedMessage * msg)
{
	SOCKET temp = msg->getSock();
	handleSignout(msg);
	closesocket(temp);

}

User * TriviaServer::handleSingin(RecievedMessage * msg)
{
	
	string userName = Helper::getStringPartFromSocket(msg->getSock(), Helper::getIntPartFromSocket(msg->getSock(), BYTES_NUM));
	string password = Helper::getStringPartFromSocket(msg->getSock(), (Helper::getIntPartFromSocket(msg->getSock(), BYTES_NUM)));
	if (this->_db.isUserAndPassMatch(userName, password))
	{
		if (this->getUserByName(userName))
		{
			// send a failure message because the user is alredy connected
			Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SIGN_IN_FAILURE_ALREADY_CONNECTED));
		}
		else
		{
			Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SIGN_IN_SECSUSS));
			this->_connectUser.insert(pair<SOCKET, User *>(msg->getSock(), new User(userName, msg->getSock())));
		}
	}
	else
	{
		// send a failure message because the userName or the password are not correct
		Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SIGN_IN_FAILURE_WRONG_DETAILS));
	}
	return nullptr;
}

bool TriviaServer::handleSignup(RecievedMessage * msg)
{
	string userName = Helper::getStringPartFromSocket(msg->getSock(), (Helper::getIntPartFromSocket(msg->getSock(), 2)));
	string password = Helper::getStringPartFromSocket(msg->getSock(), (Helper::getIntPartFromSocket(msg->getSock(), 2)));
	string mail = Helper::getStringPartFromSocket(msg->getSock(), (Helper::getIntPartFromSocket(msg->getSock(), 2)));
	if (!Validator::isPasswordValid(password))
	{
		// failure
		Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SIGN_UP_FAILURE_ILLEGAL_PASSWORD));
		return false;
	}
	if (!Validator::isUsernameValid(userName))
	{
		// failure
		Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SIGN_UP_FAILURE_ILLEGAL_USERNAME));
		return false;
	}
	if (this->_db.isUserExists(userName))
	{
		// failure
		Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SIGN_UP_FAILURE_ALREADY_EXISTS));
		return false;
	}
	else
	{
		this->_db.addNewUser(userName, password, mail);
		if (this->_db.isUserExists(userName))
		{
			//success 
			Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SIGN_UP_SECSUSS));
			return true;
		}
	}
	// failure
	Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SIGN_UP_FAILURE_OTHER_FAILURE));
	return false;
}

void TriviaServer::handleSignout(RecievedMessage * msg)
{
	User * user = getUserBySocket(msg->getSock());
	if (user != nullptr)
	{
		delete user;
		this->_connectUser.erase(msg->getSock());
		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		handleLeaveGame(msg);
	}

}

void TriviaServer::handleLeaveGame(RecievedMessage * msg)
{
	for (map<SOCKET, User *>::iterator it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		if (it->first == msg->getSock())
		{
			if (it->second->leaveGame())
			{
				delete it->second->getGame();
				it->second->setGame(nullptr);
				//delete and leave the room
			}
			break;
		}
	}

}

void TriviaServer::handleStartGame(RecievedMessage * msg)
{
	try
	{
		for (map<SOCKET, User *>::iterator it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
		{
			if (it->first == msg->getSock())
			{
				it->second->setGame(new Game(it->second->getRoom()->getUsers(), it->second->getRoom()->getQuestionsNo(), this->_db));
				this->_roomList.erase(it->second->getRoom()->getId());
				it->second->getGame()->sendFirstQuestion();
				break;
			}
		}
	}
	catch (...)
	{
		Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::CREATE_GAME_FALIURE));
	}
}

void TriviaServer::handlePlayerAnswer(RecievedMessage * msg)
{
	Game * game = nullptr;
	int answerNumber = NULL, time = NULL;
	for (map<SOCKET, User *>::iterator it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		if (it->first == msg->getSock())
		{
			game = it->second->getGame();
			if (game != nullptr)
			{
				answerNumber = Helper::getIntPartFromSocket(msg->getSock(), 1);
				time = Helper::getIntPartFromSocket(msg->getSock(), 2);
				if (!(game->handleAnswerFromUser(it->second, answerNumber, time)))
				{
					delete game;
					it->second->setGame(nullptr);
				}
			}
		}
	}



}

bool TriviaServer::handleCreateRoom(RecievedMessage * msg)
{
	string roomName = "";
	int maxUsers = NULL, questionNumber = NULL, questionTime = NULL;
	for (map<SOCKET, User *>::iterator it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		if (it->first == msg->getSock())
		{
			roomName = Helper::getStringPartFromSocket(msg->getSock(), Helper::getIntPartFromSocket(msg->getSock(), BYTES_NUM));
			maxUsers = Helper::getIntPartFromSocket(msg->getSock(), 1);
			questionNumber = Helper::getIntPartFromSocket(msg->getSock(), BYTES_NUM);
			questionTime = Helper::getIntPartFromSocket(msg->getSock(), BYTES_NUM);
			this->_roomIdSequenc++;
			if (it->second->createRoom(this->_roomIdSequenc, roomName, maxUsers, questionNumber, questionTime))
			{
				this->_roomList.insert(pair<int, Room *>(this->_roomIdSequenc, it->second->getRoom()));
				Helper::sendData(it->first, to_string(MESSAGES_CODE::CREATE_NEW_ROOM_SUCCESS));
				return true;
			}
			else
			{
				Helper::sendData(it->first, to_string(MESSAGES_CODE::CREATE_NEW_ROOM_FAILURE));
				return false;
			}
		}
	}
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage * msg)
{
	int roomNum = NULL;
	for (map<SOCKET, User *>::iterator it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		if (msg->getSock() == it->first)
		{
			roomNum = it->second->closeRoom();
			if (EROR_VALUE != roomNum)
			{
				this->_roomList.erase(roomNum);
				return true;
			}
			return false;
		}

	}

	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage * msg)
{
	int roomId = NULL;
	Room * room = nullptr;
	for (map<SOCKET, User *>::iterator it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		if (msg->getSock() == it->first)
		{
			roomId = Helper::getIntPartFromSocket(msg->getSock(), 4);
			room = getRoomById(roomId);
			if (room != nullptr)
			{
				return it->second->joinRoom(room);
			}
			return false;
		}
	}
	return false;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage * msg)
{
	for (map<SOCKET, User *>::iterator it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		if (msg->getSock() == it->first)
		{
			it->second->leaveRoom();
			return true;
		}

	}

	return false;
}

void TriviaServer::handleUserInRoom(RecievedMessage * msg)
{
	int roomId = NULL;
	Room * room = nullptr;
	for (map<SOCKET, User *>::iterator it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		if (msg->getSock() == it->first)
		{
			roomId = Helper::getIntPartFromSocket(msg->getSock(), 4);
			room = getRoomById(roomId);
			if (room != nullptr)
			{
				Helper::sendData(msg->getSock(), room->getUsersListMessage());
			}
			else
			{
				Helper::sendData(msg->getSock(), to_string(MESSAGES_CODE::SEND_USER_LIST_IN_ROOM) + "0");
			}
			break;
		}

	}
}

void TriviaServer::handleGetRooms(RecievedMessage * msg)
{
	string sendMsg = to_string(MESSAGES_CODE::SEND_ROOM_LIST)
		+= Helper::getPaddedNumber(this->_roomList.size(), 4);
	for (auto it = this->_roomList.begin(); it != this->_roomList.end(); it++)
	{
		sendMsg += Helper::getPaddedNumber(it->second->getId(), 4) 
			+ Helper::getPaddedNumber(it->second->getName().size(), 2)
			+ it->second->getName();
	}
	Helper::sendData(msg->getSock(), sendMsg);
}

void TriviaServer::handleGetBestScores(RecievedMessage * msg)
{
}

void TriviaServer::handleGetPersonalStaus(RecievedMessage * msg)
{
}


void TriviaServer::addRecievedMessage(RecievedMessage * msg)
{
	this->_queueMtx.lock();
	this->_queRcvMessages.push(msg);
	this->_queueMtx.unlock();
	this->_queueCv.notify_all();
}

RecievedMessage * TriviaServer::buildRecivedMessage(SOCKET client_socket, int msgCode)
{
	return new RecievedMessage(client_socket, msgCode);
}

User * TriviaServer::getUserByName(string userName)
{
	for (auto it = this->_connectUser.begin(); it != this->_connectUser.end(); it++)
	{
		if (it->second->getUsername() == userName)
		{
			return it->second;
		}
	}
	return nullptr;
}

User * TriviaServer::getUserBySocket(SOCKET client_socket)
{
	if (this->_connectUser.count(client_socket))
	{
		return this->_connectUser.find(client_socket)->second;
	}
	return nullptr;
}

Room * TriviaServer::getRoomById(int roomId)
{
	if (this->_roomList.count(roomId))
	{
		return this->_roomList.find(roomId)->second;
	}
	return nullptr;
}
