#include "User.h"


User::User(string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
	this->_currGame = nullptr;
	this->_currRoom = nullptr;
}
User::~User()
{

}
void User::send(string message)
{
	Helper::sendData(this->_sock, message);
}

string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room * User::getRoom()
{
	return this->_currRoom;
}

Game * User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game * game)
{
	this->_currGame = game;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;

}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{

	if (nullptr == this->_currRoom)
	{
		this->_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		//send(to_string(MESSAGES_CODE::CREATE_NEW_ROOM_SUCCESS));
		return true;
	}
	// send(to_string(MESSAGES_CODE::CREATE_NEW_ROOM_FAILURE));
	return false;
	
}

bool User::joinRoom(Room * newRoom)
{
	if (nullptr == this->_currRoom)
	{
		
		if (newRoom->joinRoom(this))
		{
			this->_currRoom = newRoom;
			return true;
		}
	}
	return false;
}

void User::leaveRoom()
{
	if (nullptr != this->_currRoom)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
		this->_currGame = nullptr;
	}


}

int User::closeRoom()
{
	int temp = NULL;
	if (nullptr == this->_currRoom)
	{
		return EROR_VALUE;
	} 
	if (temp = this->_currRoom->closeRoom(this) != EROR_VALUE)
	{
		delete this->_currRoom;
		this->_currRoom = nullptr;
	}
	return temp;
}

bool User::leaveGame()
{
	bool isFinished = false;
	if (this->_currGame != nullptr)
	{
		isFinished = this->_currGame->leaveGame(this);
	//	this->_currGame = nullptr;
	//	this->_currRoom = nullptr;
	}
	return isFinished;
}





