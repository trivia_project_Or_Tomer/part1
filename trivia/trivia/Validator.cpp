#pragma once
#include "Validator.h"

#define SPACE ' '
#define PASSWORD_CHECK_NUM 4

Validator::Validator()
{
}

Validator::~Validator()
{
}

bool Validator::isPasswordValid(string password)
{
	bool checks[PASSWORD_CHECK_NUM] = { false };
	for (int i = 0; i < password.length() && !isAllBoolTrue(checks, PASSWORD_CHECK_NUM); i++)
	{
		if (i == 3)
		{
			checks[0] = true;
		}
		if (isdigit(password[i]))
		{
			checks[1] = true;
		}
		else if (isupper(password[i]))
		{
			checks[2] = true;
		}
		else if (islower(password[i]))
		{
			checks[3] = true;
		}
		else if (SPACE == password[i])
		{
			return false;
		}
	}
	return isAllBoolTrue(checks, PASSWORD_CHECK_NUM);
}
bool Validator::isUsernameValid(string userName)
{
	return (userName.length() > 0 && isalpha(userName[0]) && userName.find_first_of(' ') == string::npos);
}

bool Validator::isAllBoolTrue(bool * arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (!arr[i])
		{
			return false;
		}
	}
	return true;
}

