#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <queue>
#include "Helper.h"
#include "DataBase.h"
#include "RecievedMessage.h"
#include "Validator.h"

using namespace std;

class RecievedMessage;
class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void server();
private:
	

	void bindAndListen();
	void accept();
	void clientHendler(SOCKET client_socket);
	void safeDeleteUser(RecievedMessage* msg);

	User * handleSingin(RecievedMessage * msg);
	bool handleSignup(RecievedMessage * msg);
	void handleSignout(RecievedMessage* msg);

	void handleLeaveGame(RecievedMessage * msg);
	void handleStartGame(RecievedMessage * msg);
	void handlePlayerAnswer(RecievedMessage * msg);



	bool handleCreateRoom(RecievedMessage * msg);
	bool handleCloseRoom(RecievedMessage * msg);
	bool handleJoinRoom(RecievedMessage * msg);
	bool handleLeaveRoom(RecievedMessage * msg);
	void handleUserInRoom(RecievedMessage * msg);
	void handleGetRooms(RecievedMessage * msg);

	void handleGetBestScores(RecievedMessage * msg);
	void handleGetPersonalStaus(RecievedMessage * msg);

	void handleRecivedMessages();
	void addRecievedMessage(RecievedMessage * msg);
	RecievedMessage * buildRecivedMessage(SOCKET client_socket, int msgCode);


	User * getUserByName(string userName);
	User * getUserBySocket(SOCKET client_socket);
	Room * getRoomById(int roomId);

// Variables 
	SOCKET _socket;
	DataBase _db;
	map<SOCKET, User *> _connectUser;
	map<int, Room *> _roomList;
	mutex _mtxReciveMessages;
	queue<RecievedMessage*> _queRcvMessages;
	static int _roomIdSequenc;

	mutex _queueMtx;
	condition_variable _queueCv;

	mutex _socketsMtx;
	condition_variable _socketsCv;
	SOCKET _comparisonSock;
};




